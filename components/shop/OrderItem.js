import React, { useState } from "react";
import { StyleSheet, View, Button, Text } from "react-native";
import CartItem from "./CartItem";

const OrderItem = (props) => {
	const [showDetails, setShowDetails] = useState(false);
    
	return (
        
		<View style={styles.orderItem}>
			<View style={styles.summary}>
				<Text style={styles.totalAmount}>
					${props.amount.toFixed(2)}
				</Text>
				<Text style={styles.date}>{props.date}</Text>
			</View>
			<Button
				title={showDetails?'Hide Details':"Show Details"}
				color={"darkred"}
				onPress={()=>{setShowDetails(prevState=>!prevState)}}
			/>

			{showDetails && (
				<View style={styles.detailItems}>
					{props.items.map(cartItem => (
						<CartItem
							key={cartItem.productId}
							quantity={cartItem.quantity}
							amount={cartItem.sum}
							title={cartItem.productTitle}
						/>
					))}
				</View>
			)}
		</View>
	);
};

const styles = StyleSheet.create({
	orderItem: {
		shadowColor: "black",
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.26,
		shadowRadius: 8,
		elevation: 6,
		borderRadius: 25,
		backgroundColor: "white",
		margin: 20,
		padding: 10,
		alignItems: "center",
	},
	summary: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%",
		marginBottom: 15,
	},
	totalAmount: {
		fontWeight: "bold",
		fontSize: 16,
	},
	date: {
		fontSize: 16,
		color: "#888",
	},
	detailItems: {
		width: "100%",
	},
});

export default OrderItem;
