import React from "react";
import {
	View,
	Text,
	Image,
	StyleSheet,
	Button,
	TouchableOpacity,
	TouchableNativeFeedback,
	Platform,
} from "react-native";

const ProductItem = (props) => {
	let TouchableComponent =
		Platform.OS === "android" ? TouchableNativeFeedback : TouchableOpacity;
	return (
		<View style={styles.product}>
			<View style={styles.touchable}>
				<TouchableComponent
					onPress={props.onSelect}
					activeOpacity={0.7}
					useForeground
				>
					<View>
						<View style={styles.imageContainer}>
							<Image
								style={styles.image}
								source={{ uri: props.image }}
							/>
						</View>
						<View style={styles.details}>
							<Text style={styles.title}>{props.title}</Text>
							<Text style={styles.price}>
								${props.price.toFixed(2)}
							</Text>
						</View>
						<View style={styles.actions}>
							{props.children}
						</View>
					</View>
				</TouchableComponent>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	product: {
		shadowColor: "black",
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.26,
		shadowRadius: 8,
		elevation: 6,
		borderRadius: 25,
		backgroundColor: "white",
		height: 300,
		margin: 30,
	},
	image: {
		width: "100%",
		height: "100%",
		overflow: "hidden",
	},
	title: {
		fontSize: 18,
		marginVertical: 4,
		fontWeight: "bold",
	},
	price: {
		fontSize: 14,
		color: "black",
	},
	actions: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		height: "25%",
		paddingHorizontal: 20,
	},
	details: {
		alignItems: "center",
		height: "15%",
	},
	imageContainer: {
		width: "100%",
		height: "60%",
		borderTopLeftRadius: 25,
		borderTopRightRadius: 25,
		overflow: "hidden",
	},
	touchable: {
		borderRadius: 25,
		overflow: "hidden",
	},
});

export default ProductItem;
