import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import productReducer from "./store/reducer/products";
import cartReducer from "./store/reducer/cart";
import ordersReducer from "./store/reducer/orders";
import ReduxThunk from "redux-thunk";
import authReducer from "./store/reducer/auth";

import AppNavigator from "./navigator/AppNavigator";

const rootReducer = combineReducers({
	products: productReducer,
	cart: cartReducer,
	orders: ordersReducer,
	auth: authReducer,
});
const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
	return (
		//here <ShopNavigator/> is replaced with NavigationContainer(renamed to (R-N v6)AppNavigator) component to acces navigation
		<Provider store={store}>
			<AppNavigator />
		</Provider>
	);
}

const styles = StyleSheet.create({});
