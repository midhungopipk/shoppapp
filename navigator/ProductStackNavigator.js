import React from "react";
import ProductOverViewScreen, {ProductOverViewScreenOptions,} from "../screens/shop/ProductOverviewScreen";
import CartScreen, { CartScreenOptions } from "../screens/shop/CartScreen";
import ProductDetailScreen, {ProductDetailScreenOptions,} from "../screens/shop/ProductDetailScreen";

import { createStackNavigator } from "@react-navigation/stack";

const defaultNavOption = {
	headerStyle: {
		backgroundColor: Platform.OS === "android" ? "red" : "",
	},
	headerTintColor: Platform.OS === "android" ? "white" : "grey",
};

const ProductStackNavigator = createStackNavigator();
export const ProductNavigator = () => {
	return (
		<ProductStackNavigator.Navigator screenOptions={defaultNavOption}>
			<ProductStackNavigator.Screen
				name="ProductsOverview"
				component={ProductOverViewScreen}
				options={ProductOverViewScreenOptions}
			/>
			<ProductStackNavigator.Screen
				name="ProductDetail"
				component={ProductDetailScreen}
				options={ProductDetailScreenOptions}
			/>
			<ProductStackNavigator.Screen
				name="Cart"
				component={CartScreen}
				options={CartScreenOptions}
			/>
		</ProductStackNavigator.Navigator>
	);
};
