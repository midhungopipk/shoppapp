import React from "react";
import { useSelector } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";

import { ShopNavigator } from "./ShopNavigator";
import { AuthNavigator } from "./AuthStackNavigator";
// import { ProductNavigator } from "./ShopNavigator";
// import { OrderNavigator } from "./ShopNavigator";
import StartUpScreen from "../screens/StartUpScreen";

const AppNavigator = (props) => {
	const isAuth = useSelector((state) => !!state.auth.token);
	const didTryAutoLogin = useSelector((state) => state.auth.didTryAutoLogin);
	console.log(isAuth, "///////////////////////");
	console.log(didTryAutoLogin, "lllllllllllllllllllllllllllllllllllllll");
	return (
		<NavigationContainer>
			{isAuth && <ShopNavigator />}
			{!isAuth && didTryAutoLogin && <AuthNavigator />}
			{!isAuth && !didTryAutoLogin && <StartUpScreen />}
		</NavigationContainer>
	);
};

export default AppNavigator;
