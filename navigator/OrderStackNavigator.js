import React from "react";

import OrderScreen, { OrderScreenOptions } from "../screens/shop/OrdersScreen";

import { createStackNavigator } from "@react-navigation/stack";

const defaultNavOption = {
	headerStyle: {
		backgroundColor: Platform.OS === "android" ? "red" : "",
	},
	headerTintColor: Platform.OS === "android" ? "white" : "grey",
};


const OrderStackNavigator = createStackNavigator();
export const OrderNavigator = () => {
	return (
		<OrderStackNavigator.Navigator screenOptions={defaultNavOption} >
			<OrderStackNavigator.Screen
				name="order"
				component={OrderScreen}
                options={OrderScreenOptions}
			/>
		</OrderStackNavigator.Navigator>
	);
};