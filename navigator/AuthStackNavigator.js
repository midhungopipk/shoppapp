import React from "react";

import AuthScreen, { AuthScreenOptions } from "../screens/user/AuthScreen";

import { createStackNavigator } from "@react-navigation/stack";

const defaultNavOption = {
	headerStyle: {
		backgroundColor: Platform.OS === "android" ? "red" : "",
	},
	headerTintColor: Platform.OS === "android" ? "white" : "grey",
};

const AuthStackNavigator = createStackNavigator();
export const AuthNavigator = () => {
	return (
		<AuthStackNavigator.Navigator screenOptions={defaultNavOption}>
			<AuthStackNavigator.Screen
				name="Auth"
				component={AuthScreen}
				options={AuthScreenOptions}
			/>
		</AuthStackNavigator.Navigator>
	);
};