import React from "react";
import {
	createDrawerNavigator,
	DrawerItemList,
} from "@react-navigation/drawer";

import { ProductNavigator } from "./ProductStackNavigator";
import { OrderNavigator } from "./OrderStackNavigator";
import { AdminNavigator } from "./AdminStackNavigator";

import { Platform, SafeAreaView, View, Button } from "react-native";

import { Ionicons } from "@expo/vector-icons";

import { useDispatch } from "react-redux";
import * as authActions from "../store/action/auth";

const defaultNavOption = {
	headerStyle: {
		backgroundColor: Platform.OS === "android" ? "red" : "",
	},
	headerTintColor: Platform.OS === "android" ? "white" : "grey",
};

const ShopDrawerNavigator = createDrawerNavigator();

export const ShopNavigator = () => {
	const dispatch = useDispatch();
	return (
		<ShopDrawerNavigator.Navigator
			drawerContent={(props) => {
				return (
					<View style={{ flex: 1, padding: 20 }}>
						<SafeAreaView forceInset={{ top: "always", horizontal: "never" }}
						>
							<DrawerItemList {...props} />
							<Button
								title="Logout"
								color="darkred"
								onPress={() => {
									dispatch(authActions.logout());
									// props.navigation.navigate("Auth");
								}}
							/>
						</SafeAreaView>
					</View>
				);
			}}
			screenOptions={defaultNavOption}
		>
			<ShopDrawerNavigator.Screen
				name="products"
				component={ProductNavigator}
				options={{
					drawerIcon: (props) => (
						<Ionicons
							name={
								Platform.OS === "android"
									? "md-cart"
									: "ios-cart"
							}
							size={25}
							color={props.color}
						/>
					),
					headerShown: false,
				}}
			/>

			<ShopDrawerNavigator.Screen
				name="Orders"
				component={OrderNavigator}
				options={{
					drawerIcon: (props) => (
						<Ionicons
							name={
								Platform.OS === "android"
									? "md-list"
									: "ios-list"
							}
							size={25}
							color={props.color}
						/>
					),
					headerShown: false,
				}}
			/>
			<ShopDrawerNavigator.Screen
				name="Admin"
				component={AdminNavigator}
				options={{
					drawerIcon: (props) => (
						<Ionicons
							name={
								Platform.OS === "android"
									? "md-create"
									: "ios-create"
							}
							size={25}
							color={props.color}
						/>
					),headerShown:false
				}}
			/>
		</ShopDrawerNavigator.Navigator>
	);
};
