import React from "react";

import UserProductScreen,{UserProductScreenOptions} from "../screens/user/UserProductScreen";
import EditProductScreen,{EditProductScreenOptions} from "../screens/user/EditProductScreen";

import { createStackNavigator } from "@react-navigation/stack";

const defaultNavOption = {
	headerStyle: {
		backgroundColor: Platform.OS === "android" ? "red" : "",
	},
	headerTintColor: Platform.OS === "android" ? "white" : "grey",
};

const AdminStackNavigator = createStackNavigator();
export const AdminNavigator = () => {
	return (
		<AdminStackNavigator.Navigator screenOptions={defaultNavOption}>
			<AdminStackNavigator.Screen
				name="UserProducts"
				component={UserProductScreen}
				options={UserProductScreenOptions}
			/>
			<AdminStackNavigator.Screen
				name="EditProduct"
				component={EditProductScreen}
				options={EditProductScreenOptions}
			/>
		</AdminStackNavigator.Navigator>
	);
};