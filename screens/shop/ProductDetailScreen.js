import React from "react";
import {
	View,
	Text,
	StyleSheet,
	Button,
	Image,
	ScrollView,
} from "react-native";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import * as CartActions from "../../store/action/cart";

const ProductDetailScreen = (props) => {
	const productId = props.route.params.productId;
	const selectedProduct = useSelector((state) =>
		state.products.availableProducts.find((prod) => prod.id === productId)
	);
	const dispatch = useDispatch();
	return (
		<ScrollView>
			<Image
				style={styles.image}
				source={{ uri: selectedProduct.imageUrl }}
			/>
			<View style={styles.action}>
				<Button
					color={"darkred"}
					title="Add to Cart"
					onPress={() => {
						dispatch(CartActions.addToCart(selectedProduct));
					}}
				/>
			</View>
			<Text style={styles.price}>
				${selectedProduct.price.toFixed(2)}
			</Text>
			<Text style={styles.description}>
				{selectedProduct.description}
			</Text>
		</ScrollView>
	);
};

export const ProductDetailScreenOptions = (navData) => {
	// const routeParams = navData.route.params ? navData.route.params : {};

	return {
		headerTitle: navData.route.params.productTitle,
	};
};

const styles = StyleSheet.create({
	image: {
		width: "100%",
		height: 300,
	},
	price: {
		fontSize: 20,
		color: "black",
		textAlign: "center",
		marginVertical: 20,
		fontWeight: "bold",
	},
	description: {
		fontSize: 18,
		textAlign: "center",
		marginHorizontal: 20,
	},
	action: {
		marginVertical: 10,
		alignItems: "center",
	},
});

export default ProductDetailScreen;
