import React, { useCallback, useEffect, useState } from "react";
import {
	View,
	Text,
	FlatList,
	Button,
	ActivityIndicator,
	StyleSheet,
} from "react-native";
import { useSelector } from "react-redux";
import ProductItem from "../../components/shop/ProductItem";
import { useDispatch } from "react-redux";
import * as CartActions from "../../store/action/cart";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../../components/UI/HeaderButton";
import { Platform } from "react-native";
import * as ProductActions from "../../store/action/products";

const ProductOverViewScreen = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState();
	const [isRefreshing, setIsRefreshing] = useState(false);
	const products = useSelector((state) => state.products.availableProducts);
	const dispatch = useDispatch();

	const loadProducts = useCallback(async () => {
		setError(null);
		setIsRefreshing(true);
		try {
			await dispatch(ProductActions.fetchProducts());
		} catch (err) {
			setError(err.message);
		}
		setIsRefreshing(false);
	}, [dispatch, setIsLoading, setError]);

	useEffect(() => {
		setIsLoading(true);
		loadProducts().then(() => {
			setIsLoading(false);
		});
	}, [dispatch, loadProducts]);

	useEffect(() => {
		const unsubscribe = props.navigation.addListener("focus", loadProducts);
		return () => {
			unsubscribe(); //listner cleanup
		};
	}, [loadProducts]);

	if (error) {
		return (
			<View style={styles.centered}>
				<Text style={{ fontWeight: "bold", fontSize: 20 }}>
					An error occured!!!
				</Text>
				<Button title="Try again" color="red" onPress={loadProducts} />
			</View>
		);
	}

	const selectItemHandler = (id, title) => {
		props.navigation.navigate("ProductDetail",{productId: id,productTitle: title,});
	};

	if (isLoading) {
		return (
			<View style={styles.centered}>
				<ActivityIndicator color={"red"} size={"large"} />
			</View>
		);
	}
	return (
		<FlatList
			onRefresh={loadProducts}
			refreshing={isRefreshing}
			data={products}
			keyExtractor={(item, index) => item.id}
			renderItem={(itemData) => (
				<ProductItem
					image={itemData.item.imageUrl}
					title={itemData.item.title}
					price={itemData.item.price}
					onSelect={() => {
						selectItemHandler(
							itemData.item.id,
							itemData.item.title
						);
					}}
					// onAddToCart={() => {
					// 	dispatch(CartActions.addToCart(itemData.item));
					// }}
				>
					<Button
						color={"darkred"}
						title="view Details"
						onPress={() => {
							selectItemHandler(
								itemData.item.id,
								itemData.item.title
							);
						}}
					/>
					<Button
						color={"darkred"}
						title="To Cart"
						onPress={() => {
							dispatch(CartActions.addToCart(itemData.item));
						}}
					/>
				</ProductItem>
			)}
		/>
	);
};

export const ProductOverViewScreenOptions = (navData) => {
	return {
		headerTitle: "All Products",
		headerRight: () => {
			return (
				<HeaderButtons HeaderButtonComponent={HeaderButton}>
					<Item
						title="Cart"
						iconName={
							Platform.OS === "android" ? "md-cart" : "ios-cart"
						}
						onPress={() => {
							navData.navigation.navigate("Cart");
						}}
					/>
				</HeaderButtons>
			);
		},
		headerLeft: () => {
			return (
				<HeaderButtons HeaderButtonComponent={HeaderButton}>
					<Item
						title="Menu"
						iconName={
							Platform.OS === "android" ? "md-menu" : "ios-menu"
						}
						onPress={() => {
							navData.navigation.toggleDrawer();
						}}
					/>
				</HeaderButtons>
			);
		},
	};
};

const styles = StyleSheet.create({
	centered: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});

export default ProductOverViewScreen;
