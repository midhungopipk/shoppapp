import React, { useEffect, useState } from "react";
import { Text, FlatList, Platform, View,ActivityIndicator ,StyleSheet} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../../components/UI/HeaderButton";
import OrderItem from "../../components/shop/OrderItem";
import * as ordersActions from "../../store/action/orders";


const OrderScreen = (props) => {
	const [isLoading, setIsLoading] = useState(false);

	const orders = useSelector((state) => state.orders.orders);
	const dispatch = useDispatch();

	useEffect(async () => {
		setIsLoading(true);
		try {
			await dispatch(ordersActions.fetchOrders());
		} catch (err) {
			throw err;
		}
		setIsLoading(false)
	}, [dispatch]);

	if(isLoading){
		return <View style={styles.centered}>
			<ActivityIndicator color={'red'} size={'large'}/>
		</View>
	}
	
	if (orders.length === 0) {
		return (
			<View style={{flex:1,justifyContent:'center',alignItems:'center',}}>
				<Text style={{fontWeight:'bold', fontSize:20,color:'darkred'}}>No orders found !!! try adding some.</Text>
			</View>
		);
	}
	return (
		<FlatList
			data={orders}
			keyExtractor={(item) => item.id}
			renderItem={(itemData) => (
				<OrderItem
					amount={itemData.item.totalAmount}
					date={itemData.item.readableDate}
					items={itemData.item.items}
				/>
			)}
		/>
	);
};

export const OrderScreenOptions = (navData) => {
	return {
		headerTitle: "Your Orders",
		headerLeft: () => {
			return (
				<HeaderButtons HeaderButtonComponent={HeaderButton}>
					<Item
						title="Menu"
						iconName={
							Platform.OS === "android" ? "md-menu" : "ios-menu"
						}
						onPress={() => {
							navData.navigation.toggleDrawer();
						}}
					/>
				</HeaderButtons>
			);
		},
	};
};

const styles=StyleSheet.create({
	centered: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
})
export default OrderScreen;
