import React ,{useState}from "react";
import {
	View,
	Text,
	FlatList,
	StyleSheet,
	Button,
	ActivityIndicator,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import CartItem from "../../components/shop/CartItem";
import * as cartActions from "../../store/action/cart";
import * as orderActions from "../../store/action/orders";

const CartScreen = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState();
	const cartTotalAmount = useSelector((state) => state.cart.totalAmount);

	const cartItems = useSelector((state) => {
		const transformedCartItemsToArray = [];
		for (const key in state.cart.items) {
			transformedCartItemsToArray.push({
				productId: key,
				productTitle: state.cart.items[key].productTitle,
				productPrice: state.cart.items[key].productPrice,
				quantity: state.cart.items[key].quantity,
				sum: state.cart.items[key].sum,
			});
		}
		return transformedCartItemsToArray.sort((a, b) =>
			a.productId > b.productId ? 1 : -1
		);
	});
	const dispatch = useDispatch();

	const sentOrderHandler = async () => {
		setIsLoading(true);
		setError(null);
		try {
			await dispatch(orderActions.addOrder(cartItems, cartTotalAmount));
		} catch (err) {
			setError(err.message);
		}
		setIsLoading(false);
	};

	
	return (
		<View style={styles.screen}>
			<View style={styles.summary}>
				<Text style={styles.summaryText}>
					Total:
					<Text style={styles.amount}>
						${Math.round(cartTotalAmount.toFixed(2) * 100) / 100}
						{/* to avoid geting the amound as negative in cart screen */}
					</Text>
				</Text>
				{isLoading ? (
					<ActivityIndicator color={"red"} size={"small"} />
				) : (
					<Button
						color={"darkred"}
						title="Order now"
						disabled={cartItems.length === 0}
						onPress={sentOrderHandler}
					/>
				)}
			</View>
			<FlatList
				data={cartItems}
				keyExtractor={(item) => item.productId}
				renderItem={(itemData) => (
					<CartItem
						quantity={itemData.item.quantity}
						title={itemData.item.productTitle}
						amount={itemData.item.sum}
						deletable
						onRemove={() => {
							dispatch(
								cartActions.removeFromCart(
									itemData.item.productId
								)
							);
						}}
					/>
				)}
			/>
		</View>
	);
};

export const CartScreenOptions = (navData) => {
	return {
		headerTitle: "Your Cart",
	};
};

const styles = StyleSheet.create({
	screen: {
		margin: 20,
	},
	summary: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		marginBottom: 20,
		padding: 10,
		shadowColor: "black",
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.26,
		shadowRadius: 8,
		elevation: 6,
		borderRadius: 25,
		backgroundColor: "white",
		height: 100,
	},
	summaryText: {
		fontWeight: "bold",
		fontSize: 18,
	},
	amount: {
		color: "darkred",
	},
	
});

export default CartScreen;
