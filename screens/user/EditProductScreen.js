import React, { useCallback, useEffect, useState, useReducer } from "react";
import {
	View,
	Text,
	StyleSheet,
	Platform,
	Alert,
	KeyboardAvoidingView,
	ActivityIndicator,
} from "react-native";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../../components/UI/HeaderButton";
import { useDispatch, useSelector } from "react-redux";
import * as productsActions from "../../store/action/products";
import Input from "../../components/UI/input";

const FORM_INPUT_UPDATES = "FORM_INPUT_UPDATES"; //1st step(useReducer)

const formReducer = (state, action) => {
	//2nd step(useReducer)
	if (action.type === FORM_INPUT_UPDATES) {
		//5th step(useReducer)
		const updatedValues = {
			...state.inputValues,
			[action.input]: action.value,
		};
		const updatedValidities = {
			...state.inputValidities,
			[action.input]: action.isValid,
		};
		let updatedFormIsValid = true;
		for (const key in updatedValidities) {
			updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
		}
		return {
			...state,
			inputValues: updatedValues,
			inputValidities: updatedValidities,
			formIsValid: updatedFormIsValid,
		};
	}
};

const EditProductScreen = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState();
	// const prodId = props.navigation.getParam("productId");
	const prodId = props.route.params?props.route.params.productId:null;
	console.log(prodId,'///////////////////////////////');
	const editedProduct = useSelector((state) =>
		state.products.userProducts.find((prod) => prod.id === prodId)
	);
	const dispatch = useDispatch();

	const [formState, dispatchFormState] = useReducer(formReducer, {
		//3rd step(useReducer)
		inputValues: {
			title: editedProduct ? editedProduct.title : "",
			imageUrl: editedProduct ? editedProduct.imageUrl : "",
			description: editedProduct ? editedProduct.description : "",
			price: "",
		},
		inputValidities: {
			title: editedProduct ? true : false,
			imageUrl: editedProduct ? true : false,
			description: editedProduct ? true : false,
			price: editedProduct ? true : false,
		},
		formIsValid: editedProduct ? true : false,
	});

	useEffect(() => {
		if (error) {
			Alert.alert("An error occured", error, [{ text: "okay" }]);
		}
	}, [error]);

	// const [title, setTitle] = useState(
	// 	editedProduct ? editedProduct.title : ""
	// );
	// const [imageUrl, setImageUrl] = useState(
	// 	editedProduct ? editedProduct.imageUrl : ""
	// );
	// const [price, setPrice] = useState("");
	// const [description, setDescription] = useState(
	// 	editedProduct ? editedProduct.description : ""
	// );
	//states for Validation
	// const [titleIsValid, setTitleIsValid] = useState();
	// const [imageIsValid, setImageIsValid] = useState();
	// const [priceIsValid, setPriceIsValid] = useState();
	// const [descriptionIsValid, setDescriptionIsValid] = useState();

	const submitHandler = useCallback(async () => {
		if (!formState.formIsValid) {
			Alert.alert(
				"Invalid input found!!!",
				"please check errors in the form",
				[{ text: "okay" }]
			);
			return;
		}
		setIsLoading(true);
		setError(null);
		try {
			if (editedProduct) {
				//Edit Mode

				await dispatch(
					productsActions.updateProduct(
						prodId,
						formState.inputValues.title, //6th step(useReducer) change every things to formstate.inputValues.title etc...
						formState.inputValues.description,
						formState.inputValues.imageUrl
					)
				);
			} else {
				//Add mode CREATING
				// if (
				// 	!titleIsValid ||
				// 	!imageIsValid ||
				// 	!priceIsValid ||
				// 	!descriptionIsValid
				// ) {
				// 	Alert.alert(
				// 		"Invalid input found!!!",
				// 		"please try to enter valid inputs",
				// 		[{ text: "okay" }]
				// 	);
				// 	return;
				// }
				await dispatch(
					productsActions.createProduct(
						formState.inputValues.title,
						formState.inputValues.description,
						formState.inputValues.imageUrl,
						+formState.inputValues.price
					)
				);
			}
			props.navigation.goBack();
		} catch (err) {
			setError(err.message);
		}
		setIsLoading(false);
	}, [dispatch, prodId, formState]);

	useEffect(() => {
		props.navigation.setOptions({
			headerRight: () => {
				return (
					<HeaderButtons HeaderButtonComponent={HeaderButton}>
						<Item
							title="Save"
							iconName={
								Platform.OS === "android"
									? "md-checkmark"
									: "ios-checkmark"
							}
							onPress={submitHandler}
						/>
					</HeaderButtons>
				);
			}
		})
	}, [submitHandler]);

	const inputChangeHandler = useCallback(
		(inputIdentifier, inputvalue, inputValidity) => {
			// let isValid = false; //4rth step(useReducer)
			// if (text.trim().length > 0) {
			// 	isValid = true;
			// 	// setTitleIsValid(false);
			// } else {
			// 	// setTitleIsValid(true);
			// }
			// setTitle(text);

			dispatchFormState({
				//4rth step(useReducer)
				type: FORM_INPUT_UPDATES,
				value: inputvalue,
				isValid: inputValidity,
				input: inputIdentifier,
			});
		},
		[dispatchFormState]
	);
	// const imageChangeHandler = (imgText) => {
	// 	if (imgText.trim().length === 0) {
	// 		setImageIsValid(false);
	// 	} else {
	// 		setImageIsValid(true);
	// 	}
	// 	setImageUrl(imgText);
	// };
	// const priceChangeHandler = (price) => {
	// 	if (price.trim().length === 0) {
	// 		setPriceIsValid(false);
	// 	} else {
	// 		setPriceIsValid(true);
	// 	}
	// 	setPrice(price);
	// };
	// const descriptionChangeHandler = (description) => {
	// 	if (description.trim().length === 0) {
	// 		setDescriptionIsValid(false);
	// 	} else {
	// 		setDescriptionIsValid(true);
	// 	}
	// 	setDescription(description);
	// };
	if (isLoading) {
		return (
			<View style={styles.centered}>
				<ActivityIndicator color={"red"} size={"large"} />
			</View>
		);
	}
	return (
		<KeyboardAvoidingView
			style={{}}
			behavior="padding"
			keyboardVerticalOffset={10}
		>
			<ScrollView>
				<View style={styles.form}>
					{/* <View style={styles.formControl}>
					<Text style={styles.label}>Title</Text>
					<TextInput
						style={styles.input}
						value={formState.inputValues.title}
						onChangeText={textChangeHandler.bind(this, "title")}
						autoCapitalize="sentences"
						autoCorrect
						returnKeyType="done"
					/>
					{/* {!titleIsValid&&<Text>Please enter everything valid...</Text>}
				</View> */}
					<Input
						id="title"
						label="title"
						keyboardType="default"
						returnKeyType="next"
						onInputChange={inputChangeHandler}
						initialValue={editedProduct ? editedProduct.title : ""}
						initiallyValid={!!editedProduct}
						errorText="please enter a valid title"
						required
					/>
					<Input
						id="imageUrl"
						label="imageUrl"
						keyboardType="default"
						returnKeyType="next"
						onInputChange={inputChangeHandler}
						initialValue={
							editedProduct ? editedProduct.imageUrl : ""
						}
						initiallyValid={!!editedProduct}
						errorText="please enter a valid imageUrl"
						required
					/>
					{editedProduct ? null : (
						<Input
							id="price"
							label="price"
							keyboardType="decimal-pad"
							returnKeyType="next"
							onInputChange={inputChangeHandler}
							errorText="please enter a price"
							required
							min={0}
						/>
					)}
					<Input
						id="description"
						label="description"
						keyboardType="default"
						returnKeyType="next"
						onInputChange={inputChangeHandler}
						autoCapitalize="sentences"
						autoCorrect
						multiline
						numberOfLines={3}
						initialValue={
							editedProduct ? editedProduct.description : ""
						}
						initiallyValid={!!editedProduct}
						errorText="please enter a valid description"
						required
						minLength={2}
					/>
				</View>
			</ScrollView>
		</KeyboardAvoidingView>
	);
};

export const EditProductScreenOptions = (navData) => {
	// const submitFn = navData.navigation.getParam("submit");
	const submitFn = navData.route.params?navData.route.params.submit:null;
	const routeParams = navData.route.params?navData.route.params:{}
	return {
		headerTitle: routeParams.productId
			? "Edit Product"
			: "Add Product",
		// headerRight: () => {
		// 	return (
		// 		<HeaderButtons HeaderButtonComponent={HeaderButton}>
		// 			<Item
		// 				title="Save"
		// 				iconName={
		// 					Platform.OS === "android"
		// 						? "md-checkmark"
		// 						: "ios-checkmark"
		// 				}
		// 				onPress={submitFn}
		// 			/>
		// 		</HeaderButtons>
		// 	);
		// },
	};
};

const styles = StyleSheet.create({
	form: {
		margin: 20,
	},
	formControl: {
		width: "100%",
	},
	label: {
		fontWeight: "bold",
		marginVertical: 10,
	},
	input: {
		paddingHorizontal: 2,
		paddingVertical: 5,
		borderBottomColor: "#ccc",
		borderBottomWidth: 2,
	},
	centered: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});
export default EditProductScreen;
