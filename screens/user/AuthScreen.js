import React, { useEffect, useReducer, useState } from "react";
import {
	ScrollView,
	StyleSheet,
	KeyboardAvoidingView,
	Button,
	View,
	ActivityIndicator,
	Alert,
} from "react-native";
import Input from "../../components/UI/input";
import Card from "../../components/UI/Card";
import { LinearGradient } from "expo-linear-gradient";
import { useDispatch } from "react-redux";
import * as authActions from "../../store/action/auth";
import { useCallback } from "react";

const FORM_INPUT_UPDATES = "FORM_INPUT_UPDATES";

const formReducer = (state, action) => {
	if (action.type === FORM_INPUT_UPDATES) {
		const updatedValues = {
			...state.inputValues,
			[action.input]: action.value,
		};
		const updatedValidities = {
			...state.inputValidities,
			[action.input]: action.isValid,
		};
		let updatedFormIsValid = true;
		for (const key in updatedValidities) {
			updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
		}
		return {
			...state,
			inputValues: updatedValues,
			inputValidities: updatedValidities,
			formIsValid: updatedFormIsValid,
		};
	}
};

const AuthScreen = (props) => {
	const [isSignup, setIsSignup] = useState(false);
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState();
	const dispatch = useDispatch();

	useEffect(()=>{
		if(error){
			Alert.alert('An Error occured',error,[{text:'okay'}])
		}
	},[error])

	const authHandler = async () => {
		let action;
		if (isSignup) {
			action = authActions.signup(
				formState.inputValues.email,
				formState.inputValues.password
			);
		} else {
			action = authActions.login(
				formState.inputValues.email,
				formState.inputValues.password
			);
		}

		setError(null);
		setIsLoading(true);
		try {
			await dispatch(action);
			// props.navigation.navigate('Shop')
		} catch (err) {
			setError(err.message);
			setIsLoading(false);
		}
		
	};

	const [formState, dispatchFormState] = useReducer(formReducer, {
		inputValues: {
			email: "",
			password: "",
		},
		inputValidities: {
			email: false,
			password: false,
		},
		formIsValid: false,
	});

	const inputChangeHandler = useCallback(
		(inputIdentifier, inputvalue, inputValidity) => {
			dispatchFormState({
				type: FORM_INPUT_UPDATES,
				value: inputvalue,
				isValid: inputValidity,
				input: inputIdentifier,
			});
		},
		[dispatchFormState]
	);
	return (
		<KeyboardAvoidingView
			behaviour="padding"
			keyboardVerticalOffset={50}
			style={styles.screen}
		>
			<LinearGradient colors={["red", "black"]} style={styles.gradient}>
				<Card style={styles.authContainer}>
					<ScrollView>
						<Input
							id="email"
							label="E-mail"
							KeyboardType="email-address"
							required
							email
							autoCapitalize="none"
							errorText="Please enter a valid e-mail address!!!"
							initialValue=""
							onInputChange={inputChangeHandler}
						/>
						<Input
							id="password"
							label="Password"
							KeyboardType="default"
							secureTextEntry
							required
							minLength={4}
							autoCapitalize="none"
							errorText="Please enter a valid Password"
							initialValue=""
							onInputChange={inputChangeHandler}
						/>

						<View style={styles.buttonContainer}>
							{isLoading ? (
								<ActivityIndicator
									size={"small"}
									color={"green"}
								/>
							) : (
								<Button
									title={isSignup ? "Signup" : "Login"}
									color={"green"}
									onPress={authHandler}
								/>
							)}
						</View>
						<View style={styles.buttonContainer}>
							<Button
								title={`Switch to ${
									isSignup ? "Login" : "Signup"
								}`}
								color={"darkorange"}
								onPress={() => {
									setIsSignup((prevState) => !prevState);
								}}
							/>
						</View>
					</ScrollView>
				</Card>
			</LinearGradient>
		</KeyboardAvoidingView>
	);
};
export const AuthScreenOptions = (navData) => {
	return {
		headerTitle: "Authenticate",
	};
};
const styles = StyleSheet.create({
	screen: {
		flex: 1,
	},
	authContainer: {
		width: "80%",
		maxWidth: 400,
		maxHeight: 400,
		padding: 20,
	},
	gradient: {
		width: "100%",
		height: "100%",
		justifyContent: "center",
		alignItems: "center",
	},
	buttonContainer: {
		marginBottom: 10,
	},
});

export default AuthScreen;
