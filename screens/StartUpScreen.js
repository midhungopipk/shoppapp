import React from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import * as authActions from "../store/action/auth";

const StartUpScreen = (props) => {
	const dispatch = useDispatch();
	useEffect(() => {
		const tryLogin = async () => {
			const userData = await AsyncStorage.getItem("userData");
			// console.log(userData,'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk')

			if (!userData) {
				// props.navigation.navigate("Auth");
				dispatch(authActions.setDidTryAutoLogin())
				return;
			}
			const transformedData = JSON.parse(userData);
			const { token, userId, expiryDate } = transformedData;

			const expirationDate = new Date(expiryDate);

			if (expirationDate <= new Date() || !token || !userId) {
				// props.navigation.navigate("Auth");
				dispatch(authActions.setDidTryAutoLogin())
				return;
			}
			const expirationTime =
				expirationDate.getTime() - new Date().getTime();

			// props.navigation.navigate("Shop");
			dispatch(authActions.authenticate(userId, token, expirationTime));
		};
		tryLogin();
	}, [dispatch]);
	return (
		<View style={styles.screen}>
			<ActivityIndicator size={"large"} color={"red"} />
		</View>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});

export default StartUpScreen;
