import CartItem from "../../models/cart";
import { ADD_TO_CART, REMOVE_FROM_CART } from "../action/cart";
import { ADD_ORDER } from "../action/orders";
import { DELETE_PRODUCT } from "../action/products";

const initialState = {
	items: {},
	totalAmount: 0,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case ADD_TO_CART:
			const addedProduct = action.product;
			const prodPrice = addedProduct.price;
			const prodTitle = addedProduct.title;

			if (state.items[addedProduct.id]) {
				//item alreadyExist
				const updatedCartItem = new CartItem(
					state.items[addedProduct.id].quantity + 1,
					prodPrice,
					prodTitle,
					state.items[addedProduct.id].sum + prodPrice
				);
				return {
					...state,
					items: {
						...state.items,
						[addedProduct.id]: updatedCartItem,
					},
					totalAmount: state.totalAmount + prodPrice,
				};
			} else {
				const newCartItem = new CartItem(
					1,
					prodPrice,
					prodTitle,
					prodPrice
				);
				return {
					...state,
					items: { ...state.items, [addedProduct.id]: newCartItem },
					totalAmount: state.totalAmount + prodPrice,
				};
			}
		case REMOVE_FROM_CART:
			const selectedCartItem = state.items[action.pid];
			const currentQty = state.items[action.pid].quantity;
			let updatedCartItem;
			if (currentQty > 1) {
				//need to reduce not to erase
				updatedCartItem = new CartItem(
					selectedCartItem.quantity - 1,
					selectedCartItem.productPrice,
					selectedCartItem.productTitle,
					selectedCartItem.sum - selectedCartItem.productPrice
				);

				updatedCartItem = {
					...state.items,
					[action.pid]: updatedCartItem,
				};
			} else {
				//need to erace not reduce
				updatedCartItem = { ...state.items };
				delete updatedCartItem[action.pid];
			}
			return {
				...state,
				items: updatedCartItem,
				totalAmount: state.totalAmount - selectedCartItem.productPrice,
			};
		case ADD_ORDER:
			return initialState; //clearing cart after placing order

		case DELETE_PRODUCT:
			if (!state.items[action.pid]) {
				return state;
			}
			const updatedItems = { ...state.items };
			const itemTotal = state.items[action.pid].sum;
			delete updatedItems[action.pid];
			return {
				...state,
				items: updatedItems,
				totalAmount: state.totalAmount - itemTotal,
			};
	}
	return state;
};
