import Product from "../../models/product";

export const DELETE_PRODUCT = "DELETE_PRODUCT";
export const CREATE_PRODUCT = "CREATE_PRODUCT";
export const UPDATE_PRODUCT = "UPDATE_PRODUCT";
export const SET_PRODUCTS = "SET_PRODUCTS";

export const fetchProducts = () => {
	return async (dispatch, getState) => {
		const userId = getState().auth.userId;
		const response = await fetch(
			"https://shopapp-c90d0-default-rtdb.firebaseio.com/products.json"
		);
		//dont have to set method (bydefault GET),headers and body for GET request.
		const resData = await response.json();
		
		const loadedProducts = [];

		for (const key in resData) {
			loadedProducts.push(
				new Product(
					key,
					resData[key].ownerId,
					resData[key].title,
					resData[key].imageUrl,
					resData[key].description,
					resData[key].price
				)
			);
		}
		if (!response.ok) {
			throw new Error("Something went wrong");
		}
		
		
		dispatch({
			type: SET_PRODUCTS,
			products: loadedProducts,
			userProducts: loadedProducts.filter(
				(prod) => prod.ownerId === userId
			),
		});
	};
};

export const deleteProduct = (productId) => {
	return async (dispatch, getState) => {
		const token = getState().auth.token;
		const response = await fetch(
			`https://shopapp-c90d0-default-rtdb.firebaseio.com/products/${productId}.json?auth=${token}`,
			{ method: "DELETE" }
		);
		if (!response.ok) {
			throw new Error("Something went wrong");
		}
		dispatch({ type: DELETE_PRODUCT, pid: productId });
	};
};

export const createProduct = (title, description, imageUrl, price) => {
	return async (dispatch, getState) => {
		const token = getState().auth.token;
		const userId = getState().auth.userId;
	

		const response = await fetch(
			`https://shopapp-c90d0-default-rtdb.firebaseio.com/products.json?auth=${token}`,
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					title,
					ownerId:userId,
					description,
					imageUrl,
					price,
					
				}),
			}
		);

		const resData = await response.json();

		dispatch({
			type: CREATE_PRODUCT,
			productData: {
				id: resData.name,
				ownerId:userId,
				title: title,
				description: description,
				imageUrl: imageUrl,
				price: price,
				
			},
		});
	};
};

export const updateProduct = (id, title, description, imageUrl) => {
	return async (dispatch, getState) => {
		const token = getState().auth.token;
		const response = await fetch(
			//here no need of response is necessary
			`https://shopapp-c90d0-default-rtdb.firebaseio.com/products/${id}.json?auth=${token}`, // Here we dont use '' or "",use only `` to tell the id in ${id}
			{
				method: "PATCH",
				header: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					title,
					description,
					imageUrl,
				}),
			}
		);
		if (!response.ok) {
			throw new Error("Something went wrong");
		}

		dispatch({
			type: UPDATE_PRODUCT,
			pid: id,
			productData: {
				title: title,
				description: description,
				imageUrl: imageUrl,
			},
		});
	};
};
