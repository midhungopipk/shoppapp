import AsyncStorage from "@react-native-async-storage/async-storage";

// export const SIGNUP = "SIGNUP";
// export const LOGIN = "LOGIN";
export const AUTHENTICATE = "AUTHENTICATE";
export const LOGOUT = "LOGOUT";
export const SET_DID_TRY_AUTOLOGIN = "SET_DID_TRY_AUTOLOGIN";

let timer;

export const setDidTryAutoLogin = () => {
	return { type: SET_DID_TRY_AUTOLOGIN };
};

export const authenticate = (userId, token, expiryTime) => {
	return (dispatch) => {
		dispatch(setLogoutTimer(expiryTime));

		dispatch({ type: AUTHENTICATE, userId: userId, token: token });
	};
};

export const signup = (email, password) => {
	return async (dispatch) => {
		const response = await fetch(
			"https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBrFGHJ7x1XQkvfLlmQkb6iP0jUq-u9mU8",
			{
				method: "POST",
				header: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					email: email,
					password: password,
					returnSecureToken: true,
				}),
			}
		);
		if (!response.ok) {
			const errorResData = await response.json();
			const errorId = errorResData.error.message;
			let message = "Something went wrong";
			if (errorId === "EMAIL_EXISTS") {
				message = "This e-mail exists already";
			}
			throw new Error(message);
		}
		const resData = await response.json();

		const idOfUser = resData.localId;
		dispatch(
			authenticate(
				idOfUser,
				resData.token,
				parseInt(resData.expiresIn) * 1000
			)
		);
		// dispatch({ type: SIGNUP, token: resData.idToken, userId: idOfUser });

		const expirationDate = new Date(
			new Date().getTime() + parseInt(resData.expiresIn) * 1000
		);

		SaveDataToStorage(resData.idToken, resData.localId, expirationDate);
	};
};

export const login = (email, password) => {
	return async (dispatch) => {
		const response = await fetch(
			"https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBrFGHJ7x1XQkvfLlmQkb6iP0jUq-u9mU8 ",
			{
				method: "POST",
				header: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					email: email,
					password: password,
					returnSecureToken: true,
				}),
			}
		);
		if (!response.ok) {
			const errorResData = await response.json();
			const errorId = errorResData.error.message;
			let message = "Something went wrong";
			if (errorId === "EMAIL_NOT_FOUND") {
				message = "This e-mail could not be found";
			} else if (errorId === "INVALID_PASSWORD") {
				message = "This Password is not valid";
			} else if (errorId === "INVALID_EMAIL") {
				message = "Please enter a valid e-mail id and password";
			}
			throw new Error(message);
		}

		const resData = await response.json();
		const idOfUser = resData.localId;
		dispatch(
			authenticate(
				idOfUser,
				resData.token,
				parseInt(resData.expiresIn) * 1000
			)
		);
		// dispatch({ type: LOGIN, token: resData.idToken, userId: idOfUser });
		const expirationDate = new Date(
			new Date().getTime() + parseInt(resData.expiresIn) * 1000
		);

		SaveDataToStorage(resData.idToken, resData.localId, expirationDate);
	};
};
//...............auto logout...............................
export const logout = () => {
	clearLogoutTimer();
	AsyncStorage.removeItem("userData");

	return { type: LOGOUT };
};

const clearLogoutTimer = () => {
	if (timer) {
		clearTimeout(timer);
	}
};

const setLogoutTimer = (expirationTime) => {
	return (dispatch) => {
		timer = setTimeout(() => {
			dispatch(logout());
		}, expirationTime);
	};
};
//..............................................................
//to save user credentials to the phone storage

const SaveDataToStorage = (token, userId, expirationDate) => {
	AsyncStorage.setItem(
		"userData",
		JSON.stringify({
			token: token,
			userId: userId,
			expiryDate: expirationDate.toISOString(),
		})
	);
};
